#!/usr/bin/python3 -I

# Author: 2024 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


from pathlib import Path
import os
import subprocess
import argparse
import argcomplete


upstream_origin = 'https://github.com/kkapsner/keepassxc-mail.git'
upstream_branch = 'master'

local_branch_upstream = 'upstream'


def configure_and_fetch_upstream(name: str, url: str, remote_branch: str, local_branch: str) -> None:

    subprocess.run(['git', 'remote', 'remove', '--', name], check = False)
    subprocess.run(['git', 'remote', 'add', '--fetch', '--master', remote_branch, '--', name, url], check = True)
    subprocess.run(['git', 'branch', '-f', local_branch, f'{name}/{remote_branch}'], check = True)


parser = argparse.ArgumentParser()
argcomplete.autocomplete(parser)
args = parser.parse_args()

os.chdir(Path(__file__).parent)

configure_and_fetch_upstream(local_branch_upstream, upstream_origin, upstream_branch, local_branch_upstream)
