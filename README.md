
Pragmatic debian packaging for KeepassXC-Mail for Thunderbird
=============================================================


See also
--------

<https://github.com/kkapsner/keepassxc-mail.git> and [README](README.upstream.md)


Building
--------

    $ dpkg-buildpackage


Obtaining current upstream version(s)
-------------------------------------

    $ ./update-upstream
